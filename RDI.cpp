#include "deps.h"

namespace fstr = std::filesystem;
using rdi = fstr::recursive_directory_iterator;

void scan(std::string, std::string, float);

int main()
{
    std::string path;
    std::string output_path;
    float timeout;
    fstr::path x;

    std::cout << "\nPlease type below the address of folder which you want to scan: ";
    std::getline(std::cin, path);

    x = fstr::path(path);
    if (!x.has_parent_path() || x.has_extension()) {
        std::cout << "Incorrect path to scan. Please refer to readme file for info";
        return -1;
    }

    std::cout << "\nPlease type below the address you want to place a report in: ";
    std::getline(std::cin, output_path);

    x = fstr::path(output_path);
    if (!x.has_parent_path()) {
        std::cout << "Incorrect output path. Please refer to readme file for info";
        return -1;
    }

    std::cout << "\nPlease type below the value of timeout: ";
    std::cin >> timeout;

    scan(path, output_path, timeout);

}


void scan(std::string path_val, std::string output_val, float timeout_val) {
    std::ofstream dir_path;
    dir_path.open(output_val);

    __int64 last_write_time_val;
    auto curr_time = std::chrono::system_clock::now();
    std::time_t curr_time_t = std::chrono::system_clock::to_time_t(curr_time);
    __int64 casted_time = static_cast<int> (curr_time_t);


    clock_t loop_time = clock();
    for (const auto& entry : rdi(path_val)) {
        if ((float)(clock() - loop_time) / CLOCKS_PER_SEC >= timeout_val) {
            std::cout << "Timeout exceeded. Function terminated. It took more than " << timeout_val << " second(s) to complete"
                << "\nLast entry time: " << (float)(clock() - loop_time) / CLOCKS_PER_SEC << " seconds" << std::endl;

            break;
        }


        last_write_time_val = std::chrono::time_point_cast<std::chrono::seconds>(fstr::last_write_time(entry.path())).time_since_epoch().count();
        dir_path << "Type : " << (entry.is_directory() ? "Directory" : "File") << ";"
            << "\nName : " << entry.path().filename() << ";"
            << "\nFull path : " << entry.path() << ";"
            << "\nParent folder : " << entry.path().parent_path() << ";"
            << "\nTotal size : " << entry.file_size() << " bytes;  "
            << "\nModification date : " << (casted_time - (last_write_time_val - 11644473600)) / 86400 << " days ago;  "
            << "\n\n";
    }

    dir_path.close();
}

