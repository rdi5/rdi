Hello :)

First of all, please use only Latin names for file and directories.
Second of all, please switch to ISO C++17 standart

To run the programm please open RDI.vcxproj file and compile it (or simply run RDI.exe)

Once you run .exe file or compile it from main file it will ask you for 3 variables.

	1. path
		Please type it as following: C:/Folder 1/folder 2/...etc

	

	2. report location
		Please type it as following: C:/Folder 1/folder2/.../*name*.csv

	
	3. timeout value
		Please type it as an usual number (or number with ".0")


After that the programm will try to recursively scan a folder *path* and write report into a specified file.
